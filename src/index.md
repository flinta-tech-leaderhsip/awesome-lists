---
layout: layouts/default.njk
title: FLINTA Tech Leadership
---
# FLINTA in Engineering Leadership Positions
Welcome to the awesome list collection of everyone in engineering / tech leadership positions who identifies themselves as:
- **F**emale
- **L**esbian
- **I**ntersexual
- **N**on binary
- **T**ranssexual
- **A** gender


Let's show the world that there ARE women in Engineering Leadership position. Let's set a sign especially for FLINTA early career people.

## Help to complete the lists
by adding them to Gitlab repository. If you don't know how to do a Merge Request, no worries. Use the [issue tracker](https://gitlab.com/flinta-tech-leadership/awesome-lists/-/issues) of the project and we will take care of the rest.
For direct contribution please visit the [project Gitlab repository](https://gitlab.com/flinta-tech-leadership/awesome-lists)


## FLINTA Tech Leadership Awesomelists
- [CTOs](./ctos/)
- [Director Engineering / Teamlead Engineering](./director-engineering/)
- [Executives Engineering / Tech ](./executives/)
- [Managing Director Engineering / Tech](./managing-directors/)
- [Vice President Engineering / Tech](./vice-presidents/)


