---
layout: layouts/default.njk
title: Executives Engineering
---
# FLINTA Executives Engineering / Tech
Everyone who identifies themselves as a gender, non-binary, non-male, women,  ... ❤️

## Adding Rules
- add by country
- add links to the profile
- if you add someone else as yourself, please ask the person before you do
- please sort alphabetically (countries and names by first name)

## Countries
- Germany
  - [Felicitas Kugland](https://www.linkedin.com/in/felicitas-kugland/), Executive Principal Engineering at SinnerSchrader, @kotzendekrabbe
