# FLINTA in Engineering Leadership Positions
Welcome to the awesome list collection of everyone in engineering / tech leadership positions who identifies themselves as:
- **F**emale
- **L**esbian
- **I**ntersexual
- **N**on binary
- **T**ranssexual
- **A** gender

## FLINTA Tech Leadership Awesomelists
Everyone who is identifies themselves as women ❤️
- [CTOs](src/ctos.md)
- [Director Engineering / Teamlead Engineering](src/director-engineering.md)
- [Executives Engineering / Tech ](src/executives.md)
- [Managing Director Engineering / Tech](src/managing-directors.md)
- [Vice President Engineering / Tech](src/vice-presidents.md)


Let's show the world that there ARE women in Engineering Leadership position. Let's set a sign especially for FLINTA early career people.

## Contributing and Code of Conduct
Everyone is welcome to contribute, regardless of age, body
size, visible or invisible disability, ethnicity, sex characteristics, gender
identity and expression, level of experience, education, socio-economic status,
nationality, personal appearance, race, caste, color, religion, or sexual identity
and orientation.

The only thing we expect is that you read and agree and the [Contributor Covenant Code of Conduct](code_of_conduct.md)

----

made with ❤️ love by Feli (@kotzendekrabbe)
